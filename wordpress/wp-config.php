<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'raulcornejo_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iKpvhT.` D!JR+hS0Cf=6T$Sa+sC(+|%Az*bEA^g+p+B1tm3w|-Z|{jVg{-bnIBB');
define('SECURE_AUTH_KEY',  'Qk&T$O7X%G)vvy1[yK/[6XzSKm:Lp+d8J[7gi9LUJLK2I++O2_PAn/zV7IU&#!F3');
define('LOGGED_IN_KEY',    '=@uup|/Wz6w7+cC+QxAQ64Izx_dMOfP*-LT3w6vMh>(3kWr3;^0YU cvA*8Pac4+');
define('NONCE_KEY',        'QJwx0T<+c#c$^#q[A@A(d4&k_l5^Y6}PZXPi:*LI]:qNqUQu1{]%QD;}@AW2%0L7');
define('AUTH_SALT',        '^&plOv{+66S`<yG2Cb;f(-rw/w0`_&cHg+uw{F2?Xtd!uAsAI4#KG#d;,%-4]t/i');
define('SECURE_AUTH_SALT', 'a4O{eloPX1!ve&@%QE`Qbt:KrThCh8i:h]?pwE}~?$<a&p8*SU&wcQk,2F)/+[|@');
define('LOGGED_IN_SALT',   'hh@mJ36I5H .:j0}XLBGLPKq&.+LR|?zFIZ9%rR;{7zI;Anw4RVNA}SC)1rPJc|G');
define('NONCE_SALT',       '|2s5O8TM[4b[Vl$(lB<vEx6r>eKj2G=|Yq0y.5xG@:!>8jK=^OVfxC%@*;~a1VG?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
